/*************************************************************
* - Responsive-UI v1.0.0 	                                 *
* - Copyright 2016, Vector008                                *
* - 8/20/2016                                                *
*************************************************************/
// ===========================================================
//  Tabs 													 =
// ===========================================================

$(document).ready(function(){
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
})